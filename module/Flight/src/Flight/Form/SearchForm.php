<?php

namespace Flight\Form;

use Zend\Form\Form;

class SearchForm extends Form {

    public function __construct($params = array()) {

        parent::__construct('searchFlight');

        $this->setAttribute('id', 'searchFlight');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', '/search');

        $this->add(array(
            'name' => 'flight_from',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                'value_options' => isset($params['flight_from']) ? $params['flight_from'] : array()
            ),
            'attributes' => array(
                'id' => 'flight_from',
                'required' => 'required'
            ),
        ));
          
        $this->add(array(
            'name' => 'flight_to',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                'value_options' => isset($params['flight_to']) ? $params['flight_to'] : array()
            ),
            'attributes' => array(
                'id' => 'flight_to',
                'required' => 'required'                
            ),
        ));                   

        $this->add(array(
            'name' => 'flight_type',
            'type' => 'Zend\Form\Element\Radio',
            'options' => array(
                'value_options' => array(
                    array(
                        'value' => 'roundtrip',
                        'label' => 'Round-trip flight',
                        'selected' => true,
                    ),
                    array(
                        'value' => 'oneway',
                        'label' => 'One Way',
                        'selected' => false,
                    )
                ),                
            ),
            'attributes' => array(
                'class' => 'flight-type',
                'required' => 'required',
            )
        ));

        $this->add(array(
            'name' => 'flight_departure',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'flight_departure',
                'placeholder' => 'Departure',
                'class' => 'is-date',
                'disabled' => 'disabled',
                'data-rule-flight-date' => 'true'                
            )
        ));
        
        $this->add(array(
            'name' => 'flight_return',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'flight_return',
                'placeholder' => 'Return',
                'class' => 'is-date',
                'disabled' => 'disabled',
                'data-rule-flight-date' => 'true' 
            )
        ));  
        
        $paxAdults = array();
        
        for ($i = 0; $i <= 9; $i++) {
            $paxAdults["$i"] = "$i Adult" . ($i === 1 ? '' : 's');
        }        
        
        $this->add(array(
            'name' => 'flight_pax_adults',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                'value_options' => $paxAdults
            ),
            'attributes' => array(
                'id' => 'flight_pax_adults',
                'data-rule-pax-required' => 'true'
            ),            
        )); 

        $paxChildren = array();
        
        for ($i = 0; $i <= 8; $i++) {
            $paxChildren["$i"] = "$i Child" . ($i === 1 ? '' : 'ren');
        }
               
        $this->add(array(
            'name' => 'flight_pax_children',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                'value_options' => $paxChildren
            ),
            'attributes' => array(
                'id' => 'flight_pax_children',
            ),              
        )); 
              
        $this->add(array(
            'name' => 'flight_pax_babies',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                'value_options' => array("0" => "0 Babies")
            ),
            'attributes' => array(
                'id' => 'flight_pax_babies',
            ),            
        ));        

        $this->add(array(
            'name' => 'search',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Search and book >',
            ),
        ));
    }

}
