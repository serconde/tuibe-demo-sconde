TUI BE – SOLLICITANT DEMO APPLICATION 
=====================================

GENERAL 
-------
Create a zendframe 2 project with a form to search our flightoffer. Allow the user to select the outbound and homebound flights they want to book and give an overview of the chosen flights. Show the flight details in this overview. Below you can find the forms or pages we expect, you can pick your own look and feel if you want. No need to exactly copy these forms. 
Use git as sourcecontrol for your project. 

DATASOURCES 
-----------
“Jetairfly B2B API” - http://dezemerel.ddns.net/api
To access these URL's you need to authenticate with basic authentication. 
Username : sollicit 
Password : ac0pX97n 
To help you with the response readability in browsers, you can add the parameter pretty=1 to each request. Don’t use this in the normal workflow, as it slows things down. 
Responses from the API are in the JSON format. 

EXPECTED FORMS  
--------------

## SEARCH A FLIGHT 
 
Fill ‘From’ and ‘To’ fields by getting the flightroutes. Once you know what route the customer wants, you can use the flightschedule call to fill the datepickers with only the available dates for the chosen route. Round-trip and one-way selection is optional. 

## PICK YOUR FLIGHT 

Use the flightavailability call to show the customer what flights are available on the chosen dates. Customer should be able to choose the flights and proceed to the next page. The buttons earlier and later are optional. 

## FLIGHT OVERVIEW 

Show the flights the customer has chosen and display the details. 
 
AVAILABLE API CALLS 
-------------------

## FLIGHTROUTES 

Get the available flightroutes. 
Base-uri : http://tstapi.jetair.be/json/1F/flightroutes/ 
QueryParams : 
Name Mandatory Info Example locale true Supported locales are nl_BE, nl_FR and en_GB nl_BE departureairport false Use this to filter the routes with the given departure airport. IATA airport code is expected. OST destinationairport false Use this to filter the routes with the given destination airport. IATA airport code is expected. ALC 

Example : 
Get all flightroutes : http://tstapi.jetair.be/json/1F/flightroutes/?locale=nl_BE 
Get all flightroutes with departure ‘OST’ : http://tstapi.jetair.be/json/1F/flightroutes/?locale=nl_BE&departureairport=OST  
 
## FLIGHTSCHEDULES 

Get the flightschedules for a specific route. 
Base-uri : http://tstapi.jetair.be/json/1F/flightschedules/ 
QueryParams : 
Name Mandatory Info Example locale true Supported locales are nl_BE, nl_FR and en_GB nl_BE departureairport true Use this to filter the outbound routes with the given departure airport. IATA airport code is expected. OST destinationairport true Use this to filter the outbound routes with the given destination airport. IATA airport code is expected. ALC returndepartureairport false Use this to filter the homebound routes with the given departure airport. IATA airport code is expected. ALC returndestiantionairport false Use this to filter the homebound routes with the given destination OST 
airport. IATA airport code is expected. 

Example : 
Get flightschedules for departure ORY->CMN and return CMN->ORY 
http://tstapi.jetair.be/json/1F/flightschedules?departureairport=ORY&destinationai rport=CMN&locale=nl_BE&returndepartureairport=CMN&returndestinationairport=ORY 

## FLIGHTAVAILABILITY 

Get the availability and flightdetails for a given itinerary. 
Base-uri : http://tstapi.jetair.be/json/1F/flightavailability/ 
QueryParams : 
Name Mandatory Info Example locale true Supported locales are nl_BE, nl_FR and en_GB nl_BE departureairport true Departure airport for the outbound flight. IATA airport code is expected. OST destinationairport true Destination airport for the outbound flight. IATA airport code is expected. ALC returndepartureairport false Departure airport for the homebound flight. IATA airport code is expected. ALC returndestiantionairport false Destination airport for the homebound flight. IATA airport code is expected. OST departuredate true Departure date, format YYYYMMDD 20160401 returndate false Return date, format YYYYMMDD 20160408 adults true Number of adults (9 max) 1 children true Number of children (9 max) 1 infants true Number of infants (don’t need an actual seat on the plane) 1 

Example : 
Get details and availability for BRU->ALC->BRU with departure on 07/04/2016 and return on 14/04/2016, traveling with 1 adult, 1 child and 1 infant. 
http://tstapi.jetair.be/json/2F/flightavailability/?departuredate=20160407&departu reairport=BRU&destinationairport=ALC&adults=1&children=1&infants=