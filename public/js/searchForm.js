$('.is-date').datepicker({dateFormat: 'dd-mm-yy'});

function disableDates(date) {
    var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
    return [enabledDates.indexOf(string) !== -1];
}

function toggleCalendars() {
    if ($('#flight_from').val() === '' || $('#flight_to').val() === '' || $('#flight_from').val() === $('#flight_to').val()) {
        $('#flight_departure').attr('disabled', 'disabled').val('');
        $('#flight_return').attr('disabled', 'disabled').val('');
    } else {


        $.ajax({
            url: '/schedule',
            method: 'POST',
            data: 'from=' + $('#flight_from').val() + '&to=' + $('#flight_to').val() + '&flightType=' + $('.flight-type:checked').val(),
            success: function (response) {
                var dates = eval('(' + response + ')');
                $('#flight_departure').datepicker('destroy');
                $('#flight_departure').datepicker({
                    dateFormat: 'dd-mm-yy',
                    beforeShowDay: function (date) {
                        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                        return [dates[0].indexOf(string) !== -1];
                    }
                });
                $('#flight_departure').datepicker('refresh');
                $('#flight_departure').removeAttr('disabled');

                if ($('.flight-type:checked').val() === 'roundtrip') {
                    $('#flight_return').datepicker('destroy');
                    $('#flight_return').datepicker({
                        dateFormat: 'dd-mm-yy',
                        beforeShowDay: function (date) {
                            var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                            return [dates[1].indexOf(string) !== -1];
                        }
                    });
                    $('#flight_return').datepicker('refresh');
                    $('#flight_return').removeAttr('disabled');
                }


            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus + ': ' + errorThrown);
            },
        });


    }
}

$('#flight_from').change(function () {

    var sel = $(this);

    $('#flight_departure').val('');
    $('#flight_return').val('');

    $.ajax({
        method: 'POST',
        data: (sel.val() === '' ? '' : 'from=' + $(this).val() + '&') + 'ajax=1',
        success: function (response) {
            var options = '<option value="">To</option>';
            var destinations = sel.val() === '' ? (eval('(' + response + ')'))[0] : eval('(' + response + ')');
            var codes = Object.keys(destinations);
            var selected = $('#flight_to option:selected').val();

            $.each(codes, function (index, code) {
                options += '<option value="' + code + '" ' + (selected == code ? 'selected' : '') + '>' + destinations[code] + '</option>';
            });

            $('#flight_to').html(options);

            if (sel.val() === '') {
                options = '<option value="">From</option>';
                var origins = (eval('(' + response + ')'))[1];
                codes = Object.keys(origins);

                $.each(codes, function (index, code) {
                    options += '<option value="' + code + '">' + origins[code] + '</option>';
                });

                $('#flight_from').html(options);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(textStatus + ': ' + errorThrown);
        },
    });

    toggleCalendars();
});

$('#flight_to').change(function () {
    var sel = $(this);

    $('#flight_departure').val('');
    $('#flight_return').val('');

    $.ajax({
        method: 'POST',
        data: (sel.val() === '' ? '' : 'to=' + $(this).val() + '&') + 'ajax=1',
        success: function (response) {
            var options = '<option value="">From</option>';
            var origins = sel.val() === '' ? (eval('(' + response + ')'))[0] : eval('(' + response + ')');
            var codes = Object.keys(origins);
            var selected = $('#flight_from option:selected').val();

            $.each(codes, function (index, code) {
                options += '<option value="' + code + '" ' + (selected == code ? 'selected' : '') + '>' + origins[code] + '</option>';
            });

            $('#flight_from').html(options);

            if (sel.val() === '') {
                options = '<option value="">To</option>';
                var destinations = (eval('(' + response + ')'))[1];
                codes = Object.keys(destinations);

                $.each(codes, function (index, code) {
                    options += '<option value="' + code + '">' + destinations[code] + '</option>';
                });

                $('#flight_to').html(options);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(textStatus + ': ' + errorThrown);
        },
    });

    toggleCalendars();
});

$('.flight-type').change(function () {
    if ($(this).val() === 'oneway') {
        $('#flight_return').attr('disabled', 'disabled').val('').hide();

    } else {
        $('#flight_return').show();
    }
    toggleCalendars();
});

$('#searchFlight').validate();

$('#flight_pax_adults').change(function () {
    var options = '<option value="0">0 Babies</option>';

    if ($(this).val() !== '' && $(this).val() != '0') {
        for (var i = 1; i <= $(this).val(); i++) {
            options += '<option value="' + i + '">' + i + ' Bab' + (i === 1 ? 'y' : 'ies') + '</option>';
        }
    }

    $('#flight_pax_babies').html(options);
});

jQuery.validator.addMethod("pax-required", function (value, element) {
    return $('#flight_pax_adults').val() != 0 || $('#flight_pax_children').val() != 0;
}, "Select amount of passengers");

jQuery.validator.addMethod("flight-date", function (value, element) {
    var result = false;
    if ($('#flight_departure').val()==='') {
        return false;
    }
    
    if ($('.flight_type:checked').val()==='roundtrip' && $('#flight_return').val()==='') {
        return false;
    }
    $.ajax({
        url: '/checkdates',
        async: false,
        method: 'POST',
        data: 'depDate=' + $('#flight_departure').val() + '&retDate=' +  $('#flight_return').val(),
        success: function (response) {
            var r = eval ('(' + response + ')');
            result = r.ok;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(textStatus + ': ' + errorThrown);
        },        
    });
    
    return result;
}, "Choose valid flight dates");